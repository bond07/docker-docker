ARG BASE_IMAGE_PREFIX=docker.io

FROM ${BASE_IMAGE_PREFIX}/alpine

FROM gitlab.com:443/gitlab-gold/dependency_proxy/containers/busybox:latest

FROM gitlab.com:443/gitlab-gold/dependency_proxy/containers/hello-world
